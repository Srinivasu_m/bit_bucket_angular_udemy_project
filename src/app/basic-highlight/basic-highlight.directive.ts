import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[appBasicHighlight]' // If using [] in this selector place, 
    //we can just call this directive name without [] in the template file. Just like appBasicHighlight
})

export class BasicHighlightDirective implements OnInit {

    constructor(private elementRef: ElementRef){

    }

    ngOnInit(){
        this.elementRef.nativeElement.style.backgroundColor = "green";
        this.elementRef.nativeElement.style.margin = "10px 0px";
        this.elementRef.nativeElement.style.color = "white";
        this.elementRef.nativeElement.style.fontWeight = "bold";
    }

}