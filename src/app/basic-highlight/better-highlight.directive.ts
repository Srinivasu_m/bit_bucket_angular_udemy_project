import { Directive, Renderer2, OnInit, ElementRef, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  constructor(private renderer: Renderer2, private elRef: ElementRef) { }

  @HostBinding('style.backgroundColor') backgroundColor:string = "transparent";

  ngOnInit(){
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color','blue');
    // this.renderer.setStyle(this.elRef.nativeElement, 'color','white');
  }

  @HostListener('mouseenter') mouseenter(eventData: Event)
  {
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color','yellow');
    this.backgroundColor = 'yellow';
  }



  @HostListener('mouseleave') mouseleave(eventData: Event)
  {
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color','transparent');
    this.backgroundColor = 'transparent';
  }

  


}
