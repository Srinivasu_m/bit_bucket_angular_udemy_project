import { Component, OnInit  } from '@angular/core';

@Component({
  selector: 'app-check-uncheck',
  templateUrl: './check-uncheck.component.html',
  styleUrls: ['./check-uncheck.component.css']
})
export class CheckUncheckComponent implements OnInit {

  title = 'Angular 7 CheckBox Select/ Unselect All';
  masterSelected:boolean;
  checklist:any;
  checkedList:any;
  items= false;
  selectedList:string;
  selectedListString:string = '';
  prevSelectedListString:string = '';
  proceed:string = '';
  

  //regions:String[];
  regions = ['region 1', 'region 2', 'region 3'];
  selected_checkbox = [];
 
  constructor(){
      this.masterSelected = false;
  
      this.checklist = [
        {id:1,value:'Elenor Anderson',isSelected:false},
        {id:2,value:'Caden Kunze',isSelected:false},
        {id:3,value:'Ms. Hortense Zulauf',isSelected:false},
        {id:4,value:'Grady Reichert',isSelected:false},
        {id:5,value:'Dejon Olson',isSelected:false},
        {id:6,value:'Jamir Pfannerstill',isSelected:false},
        {id:7,value:'Aracely Renner DVM',isSelected:false},
        {id:8,value:'Genoveva Luettgen',isSelected:false}
      ];
      this.getCheckedItemList();
  }
 
  checkUncheckAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.checklist.every(function(item:any) {
        return item.isSelected == true;
      })
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected)
      this.checkedList.push(this.checklist[i]);
    }
    
    this.checkedList = JSON.stringify(this.checkedList);
  }

  ngOnInit() {
  }


  updateItem(eve:any)
  {




    //this.selected_checkbox = [];
    this.prevSelectedListString = (this.selected_checkbox).toString();

    for (let chart of this.regions) {
        if (eve.target.value === chart && <HTMLInputElement>eve.target.checked) {
          //this.items = true;
          this.selected_checkbox.push(chart);
        } 
        
        if(eve.target.value === chart && !<HTMLInputElement>eve.target.checked)
        {
          const index: number = this.selected_checkbox.indexOf(chart);
          if (index !== -1) {
            this.selected_checkbox.splice(index, 1);
          }  
        }
    }
    if(this.selected_checkbox.length > 0)
    {
      this.items = true;
    } else {
      this.items = false;
    }

    this.selectedList = (this.selected_checkbox).toString();//(",");
    this.selectedListString = (this.selected_checkbox).toString();//(",");
    this.proceed = 'changed';
  }


  consoleData(e:any)
  {
    this.proceed = (this.proceed == 'changed') ? 'proceed' : 'no';
    if(e.relatedTarget == null && this.selected_checkbox.length > 0 && (this.selectedListString != this.prevSelectedListString) && this.proceed == 'proceed')
    {
      console.log("Make AJAX Call");
    }
    //console.log(e.srcElement.attributes.id);
  }

}
