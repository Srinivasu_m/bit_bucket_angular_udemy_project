import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';


@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent implements OnInit {

  name = 'Angular 6';
  column:String;
  sortCols:OrderCondition[];
  // isDesc:boolean;
  records= [
      { ReferenceId: 10, CategoryID: 1,  CategoryName: "Beverages", Description: "Coffees, teas" },
      { ReferenceId: 8, CategoryID: 2,  CategoryName: "Condiments", Description: "Sweet and savory sauces" },
      { ReferenceId: 12, CategoryID: 3,  CategoryName: "Confections", Description: "Desserts and candies" },
      { ReferenceId: 56, CategoryID: 4,  CategoryName: "Cheeses",  Description: "Smetana, Quark and Cheddar Cheese" },
      { ReferenceId: 47, CategoryID: 5,  CategoryName: "Grains/Cereals", Description: "Breads, crackers, pasta, and cereal" },
      { ReferenceId: 15, CategoryID: 6,  CategoryName: "Beverages", Description: "Beers, and ales" },
      { ReferenceId: 85, CategoryID: 7,  CategoryName: "Condiments", Description: "Selishes, spreads, and seasonings" },
      { ReferenceId: 43, CategoryID: 8,  CategoryName: "Confections", Description: "Sweet breads" },
      { ReferenceId: 76, CategoryID: 9,  CategoryName: "Cheeses",  Description: "Cheese Burger" },
      { ReferenceId: 5, CategoryID: 10, CategoryName: "Grains/Cereals", Description: "Breads, crackers, pasta, and cereal" }
     ];
    ngOnInit(){
    this.column='ReferenceId';
    this.sortCols = [];
    }
     sort(property){
        // this.isDesc = !this.isDesc; //change the direction   
        let tmp = this.column.length?this.column.split(','):[];
        const i = _.indexOf(tmp,property); 
        if( i >=0){
           tmp.splice(i,1);
        }else{
          tmp.push(property);
        }
        this.column = tmp.join(',');
        
        let selectCol = _.find(this.sortCols,{name:property});
        if(selectCol){
          selectCol.click();
          let index = _.findIndex(this.sortCols,{name:property});
          this.sortCols.splice(index,1,selectCol);
        }else{
          this.sortCols.push(new OrderCondition(property))
        }
    };

}

export class OrderCondition{
  name:string;
  direction:number;
  clicked:number;
  constructor(name){
    this.name = name;
    this.clicked = 1;
    this.direction = 1; //asc
  }
  click(){
    this.clicked++;
    this.chechDirection();
  }
  chechDirection(){
    const mod = this.clicked % 3;
    switch(mod){
      case 0:
        this.direction = 0;
      break;
      case 1:
        this.direction = 1;
      break;
      case 2:
        this.direction = -1;
      break
    }
  }
}