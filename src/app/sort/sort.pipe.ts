import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
import {OrderCondition} from './sort.component';

@Pipe({  name: 'orderBy' ,pure: false})
export class OrderrByPipe implements PipeTransform {

    transform(records: Array<any>, args?: any): any {
        console.log(args);
        // return _.sortBy(records,args.property)
        for(let c of args){
          records = this.sortByCondition(records,c);
        }
        return records;
    };

    sortByCondition(record: Array<any>, condition:OrderCondition){
      let tmp = [];
      switch(condition.direction){
        case 0:
        tmp= record;
        break;
        case 1:
        tmp= _.sortBy(record,[condition.name]);
        break;
        case -1:
        tmp = _.reverse(_.sortBy(record,[condition.name]));
        break;
      }
      return tmp;
    }
}