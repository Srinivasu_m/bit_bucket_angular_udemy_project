import { Directive, HostListener, HostBinding, ElementRef } from '@angular/core';

@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective {
    // HostLinstener used to perform an event to the DOM element where this directive is called
    //HostBinding is used to set the value back to the DOM element

    /*
    // BELOW CODE IS USED TO OPEN THE DROP DOWN MENU ITEM, BUT CLOSING IS HAPPENS WHEN CLICKED AGAIN ON LINK
    @HostBinding('class.open') isOpen = false; // Here we are binding class = 'open' to the
    // DOM element dynamically using HostBinding concept

    //Capturing an 'click' event on the DOM to perform some action
    @HostListener('click') toggleOpen()
    {
        this.isOpen = !this.isOpen;
    }
*/
    // NEW CODE TO OPEN AND CLOSE
    @HostBinding('class.open') isOpen = false;

    @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
        this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false;
    }
  constructor(private elRef: ElementRef) {}




}