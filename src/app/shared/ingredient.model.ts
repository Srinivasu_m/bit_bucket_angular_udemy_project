export class Ingredient {
    /*
    ONE WAY OF SETTING UP PROPERTIES IN MODEL
    public name:string;
    public amount:number;

    constructor(name: string, amount: number){
        this.name = name;
    }

    Below is another way of setting the Properties, by just adding the specificer infront
    of variable name as shown below
    */
   constructor(public name:string, public amount:number){}

}