import { Injectable } from '@angular/core';
import { LoggingService } from './logging.service';

@Injectable()

export class ReportService {

    constructor(private loggingServ: LoggingService){}

    logData(componentName:string){
        this.loggingServ.logMessage(componentName);
        //console.log('Requested called from component ' + componentName );
    }
}