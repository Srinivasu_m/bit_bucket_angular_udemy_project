import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
//import { stringify } from '@angular/core/src/render3/util';
//import { variable } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-modal-popup',
  templateUrl: './modal-popup.component.html',
  styleUrls: ['./modal-popup.component.css']
})
export class ModalPopupComponent implements OnInit {

  date:Date; //date Variable
  logedInForm:any; //These are variables
  emailId:string;
  password:any;
  display='none'; //default Variable
  navItems:any;

  constructor() { }

  ngOnInit() {
    this.date = new Date(); // Today date and time
    //Login Validation
    this.logedInForm = new FormGroup({
      emailId: new FormControl("",
        Validators.compose([
          Validators.required,
          Validators.pattern("[^ @]*@[^ @]*")
      ])),
      password: new FormControl('YourPassword', [
           Validators.minLength(8),
           Validators.required])
    });
  }

  // Model Driven Form - login
  mdfLogin(data) {
    this.emailId = data.emailId;
    this.password = data.password;
    //JSON.stringify(data)
    alert(data.emailId);
  }

  openModalDialog(){
    
    this.display='block'; //Set block css
 }

 closeModalDialog(){
  this.display='none'; //set none css after close dialog
  //console.log("modal pop closed");

  
  

 }

 showError(fldVal: string)
    {
      console.log(this.logedInForm);
    }
}


