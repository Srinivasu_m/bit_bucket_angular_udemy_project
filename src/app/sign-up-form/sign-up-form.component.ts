import { Component, OnInit } from '@angular/core';

// Below code is used for FormControl form validation
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';


import { User } from '../User';


@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.css']
})
export class SignUpFormComponent implements OnInit {

  //Gender list for the select control element
  private genderList: string[];
  //Property for the user
  private user:User;
  signupForm: FormGroup;
  entries:any = [];
/* FormControl with initial value and a validator */
  email = new FormControl('bob@example.com', Validators.required);
  year = ['2018','2019','2020'];
  

  constructor(private fb:FormBuilder) {}

  ngOnInit() {

    this.genderList =  ['Male', 'Female', 'Others'];


    // get email() { return this.signupForm.get('email'); }
     
    // get password() { return this.signupForm.get('password'); }
 
    // get gender() { return this.signupForm.get('gender'); }
 
    // get terms() { return this.signupForm.get('terms'); }

    this.signupForm = this.fb.group ({
      email: new FormControl('',[Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      password: this.fb.group ({
        pwd: new FormControl('',[Validators.required, Validators.minLength(8)]),
        confirmPwd: new FormControl('',[Validators.required, Validators.minLength(8)])
    }),
      // pwd: new FormControl('',Validators.required),
      // confirmPwd: new FormControl('',Validators.required),
      gender: new FormControl('',Validators.required),
      terms: new FormControl('',Validators.required),
      year: new FormControl('',Validators.required),
      person: new FormControl('',Validators.required)
  })
  }


  public onFormSubmit() {
    if(this.signupForm.valid) {
        this.user = this.signupForm.value;
        console.log(Number(this.signupForm.value.year));
        console.log(this.user);
        /* Any API call logic via services goes here */
    }
  } 


    

}
