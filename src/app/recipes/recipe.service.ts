import { EventEmitter, Injectable } from '@angular/core';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model'
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {

    constructor(private slService: ShoppingListService){

    }

    recipeSelected = new EventEmitter<Recipe>();



    // Setting up the recipe data
    recipes:Recipe[] = [
        new Recipe('Test recipe-1', 'This is a simply test recipe', 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/healthy_kids_main_image.jpg',
        [
          new Ingredient('Chicken',1),
          new Ingredient('Eggs', 10)
        ]
        ),
        new Recipe('Test recipe-2', 'This is a simply test recipe','https://www.inspiredtaste.net/wp-content/uploads/2018/12/Sauteed-Zucchini-Recipe-1-1200.jpg',
        [
          new Ingredient('meat',1),
          new Ingredient('Butter', 10)
        ]
        )
      ];


      //since we shouldn't allow the users to access the recipe Array object directly from outside,
      // we create a function to that.
      getRecipes(){
        // SLICE will return the new array which is copy of the above recipes
        return this.recipes.slice();
      }


      addIngredientsToShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients);
      }




}