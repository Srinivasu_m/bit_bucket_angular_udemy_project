// 2nd way of using service for cross component communication
import { Component, OnInit, Input} from '@angular/core';


import { Recipe } from '../../recipe.model';

// calling the component level service that is been created
import { RecipeService } from '../../recipe.service'; 



@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {

  @Input() recipe: Recipe;

  

  // 2nd way of using service for cross component communication
   constructor(private recipeService: RecipeService){

   }


  ngOnInit() {
  }

  onSelected()
  {
    // 2nd way of using service for cross component communication
    this.recipeService.recipeSelected.emit(this.recipe);
  }

}
