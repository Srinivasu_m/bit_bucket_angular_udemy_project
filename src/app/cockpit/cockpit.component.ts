import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {

  @Output() serverCreated = new EventEmitter<{serverName:string, serverContent:string}>();
  @Output('bpCreated') blueprintCreated = new EventEmitter<{serverName:string, serverContent:string}>();
  constructor() { }


 


  ngOnInit() {
  }

  
  //newServerName = '';
  //newServerContent = '';

  // In the below line, we can pass component name aswell but without single quote.
  @ViewChild('serverContentInput', {static:false}) serverContentInput: ElementRef;

  onAddServer(serverNameInput: HTMLInputElement) {
    
    
    //Below lines of code is a way of two-way data binding.
    this.serverCreated.emit({
      serverName: serverNameInput.value,//this.newServerName, 
      serverContent: this.serverContentInput.nativeElement.value//this.newServerContent
    });
    //TWO - WAY data binding ends, refer other way of binding. i.e template local reference way

    
    
    //template local reference
    // this.serverCreated.emit({
    //   serverName: serverNameInput.value,
    //   serverContent: this.newServerContent
    // });



    // this.serverElements.push({
    //   type: 'server',
    //   name: this.newServerName,
    //   content: this.newServerContent
    // });
  }

  onAddBlueprint(serverNameInput: HTMLInputElement) {
    //console.log(this.serverContentInput); // part of viewchild
    // this.serverElements.push({
    //   type: 'blueprint',
    //   name: this.newServerName,
    //   content: this.newServerContent
    // });

    
    // //Below lines of code is a way of two-way data binding.
    this.blueprintCreated.emit({
      serverName: serverNameInput.value,//this.newServerName, 
      serverContent: this.serverContentInput.nativeElement.value//this.newServerContent
    });
    //TWO - WAY data binding ends, refer other way of binding. i.e template local reference way
    
  //  this.blueprintCreated.emit({
  //     serverName: serverNameInput.value, 
  //     serverContent: this.newServerContent
  //   });
  }


}
