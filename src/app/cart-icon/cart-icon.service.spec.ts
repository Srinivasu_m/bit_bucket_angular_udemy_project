import { TestBed } from '@angular/core/testing';

import { CartIconService } from './cart-icon.service';

describe('CartIconService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartIconService = TestBed.get(CartIconService);
    expect(service).toBeTruthy();
  });
});
