import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

//importing services:
//import { LoggingService } from '../shared/services/logging.service'; 
import { ReportService } from '../shared/services/Report.service'; // already object initiated from app component
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
  //providers: [LoggingService], making this Service available globally by defining in AppModule.ts
})
export class ShoppingListComponent implements OnInit {

  ingredients: Ingredient[];

  

  //const logService = new LoggingService(); // WRONG APPROACH OF INITIALIZING THE SERVICE OBJECT
  // NEW APPROACH initializing the Service instance inside a constructor without the NEW keyword.
 
  constructor(
    //private loggingService: LoggingService, 
    private reportService: ReportService,
    private slService: ShoppingListService) { }

  ngOnInit() {
    //this.reportService.logData('Shopping List Component');
    this.ingredients = this.slService.getIngredients();
    this.slService.ingridentsChanged.subscribe((ingredients: Ingredient[])=>{
      this.ingredients = ingredients;
    });
  }



  

}
