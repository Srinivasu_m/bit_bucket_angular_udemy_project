import { EventEmitter } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';

export class ShoppingListService{

    ingridentsChanged = new EventEmitter<Ingredient[]>();

    private ingredients: Ingredient[] = [
        new Ingredient('Rice',10),
        new Ingredient('Tomatos',5)
      ];

      getIngredients(){
          return this.ingredients.slice();
      }

      addIngredient(ingredient: Ingredient){
        this.ingredients.push(ingredient);
        this.ingridentsChanged.emit(this.ingredients.slice());
      }

      addIngredients(ingredients: Ingredient[]){
        //to push array of objects into an array. we have 2 ways:
        /* 1st method, loop through the array and push each item into an master array:
        
        for(let ingredient of ingredients){
          this.addIngredient(ingredient);
        }
        */
        //2nd method is :
        this.ingredients.push(...ingredients);
        // post pushed the new item to shopping cart. we need to emit the updated array:
        this.ingridentsChanged.emit(this.ingredients.slice());
      }

}