import { Component, ViewEncapsulation, HostListener } from '@angular/core';

//Importing Service ReportService into the component
import { ReportService } from './shared/services/Report.service';
import { UserService } from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', 
  /*This is using an External HTML file. We can even use custom HTML code
   by using the template: `(back-tick symbol)=> to write code in multiple lines <div>hello </div>` */
  styleUrls: ['./app.component.css'],
  /* 
  Just like template, we can write custom CSS within the .ts file 
  by using styles: [`
    h3 { color:red}
  `]
  */

  /*
  To remove special text in the DOM elements like "_ngcontent-ejo-X" we can use the encapsulation concept
  as shown below:None/Native (Shadow dom technology)/Emulated
  */
  encapsulation: ViewEncapsulation.None,

  //providers: [ReportService]
  providers: [UserService]


})
export class AppComponent {

  constructor(private reportServ:ReportService){}


  /*
  title = 'My Angular Project';


  public name:string;
  public years:any;
  public isDiabled:boolean;
  public unamePattern:any;
  
  public displayMsg = false;
  log = [];

  ngOnInit(){
    this.name = "Srinivasu";
    this.isDiabled = true;
 
    //const pattern = /(^\d*\.?\d*[1-9]+\d*$)|(^[1-9]+\d*\.\d*$)/;
  }

  public enableBtn(){
    this.isDiabled = false;
  }

  //Update cart function - Two way binding
  updateCartValue(selectedVal){
    console.log(selectedVal);
  }

  displayMessage(){
    this.displayMsg = !this.displayMsg;
//    this.log.push(this.log.length + 1);

//we will push timestamp 
this.log.push(new Date());

  }
*/



/*
//Below code for modal pop
display:string;
loaderDisplay:string;
ngOnInit(){
  this.display='block';
  this.loaderDisplay="none";
}

  onCloseHandled(){
    this.loaderDisplay="block";
    setTimeout(() => {
          this.display='none';      
    }, 2000);

   }

  openModal(){
    this.display='block';
  }
  */


  oddNumbers: number[]= []; //assignment number 81
  evenNumbers: number[]= []; // belongs to assignment #81
  years:number[] = [];
  months:string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  displayMonths:string[] = [];
  loadedFeature = 'recipe'; //Shopping cart functionality


  /*
  Code to test for no records available in table using ng-container directive
  */
 quotaDashboard = [];


 serverElements = [{name:'test',type:'server',content:'this is test server'}];

 onServerAdded(serverData:{serverName:string, serverContent:string}){

  this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }

  onBlueprintAdded(bluePrintData:{serverName:string, serverContent:string}) {
    this.serverElements.push({
      type: 'blueprint',
      name: bluePrintData.serverName,
      content: bluePrintData.serverContent
    });
  }

  onChangeFirst(){
    this.serverElements[0].name="Changed!";
  }


  onDestroyFirst()
    {
      this.serverElements.splice(0, 1);
    }

    onIntervalFired(firedNumber: number){
      if(firedNumber % 2 === 0){
        this.evenNumbers.push(firedNumber);
      } else{
        this.oddNumbers.push(firedNumber);
      }
    }

    onNavigation(feature: string){
      this.loadedFeature = feature;
    }

    //Using *ngSwitch directive
    value:number= 1000000;
   
    ngOnInit()
    {

       //this.years = [2019, 2020, 2021, 2022];
       this.ProcessYearMonths();
      this.reportServ.logData("App component");
    }

    ProcessYearMonths()
    {
        let currYear = new Date().getFullYear();
        let currMonth = new Date().getMonth();
        let currDate = new Date().getDate();
        if(currDate > 22)
        {
          //IF current month is greater than OCT
          if(currMonth > 9 ) 
          {
            // add +1 to current year
            currYear = currYear+1;
          } 
          // add +2 to current month
          currMonth = currMonth+2;
        } else {
          if(currMonth == 11 ) // if current month is DEC 
          {
            // add +1 to current year
            currYear = currYear+1;
          }
          // add +1 to current month
          currMonth = currMonth+1; 
        }
        for(let i=0; i<4;i++)
        {
          this.years.push(currYear);
          currYear++;
        }
        currMonth = (currMonth > 11) ? currMonth-11 : currMonth;
        for(let m=currMonth; m<=12; m++)
        {
          console.log(m);
          this.displayMonths.push(this.months[m-1]);
        }
    }


    /*
    BELOW CODE IS EXAMPLE FOR SHARING THE DATA BETWEEN COMPONENTS USING
    SERVICES
     
    WE replaced below code with Services

    activeUsers = ['Max', 'Anna'];
    inactiveUsers = ['Chris', 'Manu'];

    onSetToInactive(id: number) {
      this.inactiveUsers.push(this.activeUsers[id]);
      this.activeUsers.splice(id, 1);
    }

    onSetToActive(id: number) {
      this.activeUsers.push(this.inactiveUsers[id]);
      this.inactiveUsers.splice(id, 1);
    }

    BELOW CODE IS EXAMPLE FOR SHARING THE DATA BETWEEN COMPONENTS USING
    SERVICES - ends here
    */
   @HostListener ('window:beforeunload', ['$event'] ) 
   beforeUnloadHander(event: Event){
    //unloadHandler(event){
     //this.logme();
   }

   logme(){
     let currDTS = new Date();

     localStorage.setItem("closed","'"+currDTS+"'");
     console.log("browser closed");
     return true;
   }

}
