import { 
  Component, 
  OnInit, 
  Input, 
  OnChanges, 
  SimpleChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements 
OnInit, 
OnChanges,
DoCheck, 
AfterContentInit,
AfterContentChecked,
AfterViewInit,
OnDestroy  {

  @Input('srvElement') element: {name:string, type:string, content:string};
  @Input() name:string;
  @ViewChild('heading', {static:true}) header: ElementRef;
  @ContentChild('contentParagraph', {static:true}) paragraph:ElementRef;
  constructor() { 

    console.log('ServerElementComponent Constructor called');
  }

  ngOnInit() {
    console.log('ServerElementComponent ngOnInit called');
    console.log("TEXT display"+ this.header.nativeElement.textContent);
    console.log("TExt paragraph : "+this.paragraph.nativeElement.textContent);
  }

  ngOnChanges(changes: SimpleChanges){
    console.log('ServerElementComponent ngOnChanges called');
    console.log(changes);
  }

  ngDoCheck()
  {
    console.log('ServerElementComponent ngDoCheck called');
  }

  ngAfterContentInit(){
    console.log('ServerElementComponent ngAfterContentInit called');
    console.log("TExt paragraph : "+this.paragraph.nativeElement.textContent);
  }

  ngAfterContentChecked()
  {
    console.log('ServerElementComponent ngAfterContentChecked called');
  }

  ngAfterViewInit(){
    console.log('ServerElementComponent ngAfterViewInit called');
    console.log("TEXT display"+ this.header.nativeElement.textContent);
  }

  ngOnDestroy(){
    console.log('ServerElementComponent ngOnDestroy called');
  }
}
