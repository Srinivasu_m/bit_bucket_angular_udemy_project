import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  // template: `
  //   Message: {{ message }}
  //   <app-child></app-child>
  // `,
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  display:string = 'none';

  
  constructor() { }
  
  ngOnInit() {
  }

  openModal(){
    this.display='block';
  }

  receiveMessage($event:any)
  {
    this.display=$event;

  }

 

}
