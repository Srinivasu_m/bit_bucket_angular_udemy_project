import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers', // Default way of defining component selector to use in UI.
//selector: '[app-servers]', // Attribute way of defining component selctor
//selector: '.app-servers', // defining the component selector using CSS selector method
  templateUrl: './servers.component.html',
  //styleUrls: ['./servers.component.css']
  styles:[`
    .errorMsg { 
      color:blue;
    }
  `]
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreatedStatus = 'No server is created yet!';
  disabled = false;
  inputVal = '';
  serverCreatedFlag = false;
  username:string = '';
  servers = [];



  constructor() { 
    setTimeout(()=>{
      this.allowNewServer = true;
    },2000)
  }

  ngOnInit() {

    if((this.username).length > 0)
    {
      console.log(this.username);
      this.disabled =true;
    }

  }

  createServer(){
    this.serverCreatedFlag = true;
    this.serverCreatedStatus = this.inputVal;//"Server created successfully";
    this.servers.push(this.inputVal);
  }

  onUpdateServerName(event:Event)
  {
    this.inputVal = (<HTMLInputElement>event.target).value;
    console.log(this.inputVal);
  }


  clearUserName()
  {
    console.log("Clear usernamen btn clicked");
    this.username = '';
  }

  getColor()
  {
    return this.serverCreatedFlag ? 'green' : 'red';
  }





}
