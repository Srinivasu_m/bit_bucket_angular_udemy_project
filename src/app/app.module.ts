import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Below import section is used for form elements
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Below import section is used for form elements - end
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { ServersComponent } from './servers/servers.component';
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { WarningalertComponent } from './warningAlert/warningalert.component';
import { ModalPopupComponent } from './modal-popup/modal-popup.component';
import { SortComponent } from './sort/sort.component';
//import {MatButtonModule,MatTooltipModule} from '@angular/material';


//sort 
import{OrderrByPipe} from './sort/sort.pipe';
import { CartIconComponent } from './cart-icon/cart-icon.component';
import { CheckUncheckComponent } from './check-uncheck/check-uncheck.component';


import { HeaderComponent } from './header/header.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './recipes/recipe-list/recipe-item/recipe-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { CockpitComponent } from './cockpit/cockpit.component';
import { ServerElementComponent } from './server-element/server-element.component';
import { GameControlComponent } from './game-control/game-control.component';
import { OddComponent } from './odd/odd.component';
import { EvenComponent } from './even/even.component';

// Custom directive we created for backgroundColor:green.
import { BasicHighlightDirective } from './basic-highlight/basic-highlight.directive';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { BetterHighlightDirective } from './basic-highlight/better-highlight.directive';
import { DropdownDirective } from './shared/dropdown.directive';

// Injecting the service to make available across the application
import { ReportService } from 'src/app/shared/services/Report.service';
import { LoggingService } from 'src/app/shared/services/logging.service';
import { RecipeService } from './shared/services/recipe.service';
import { ActiveUsersComponent } from './active-users/active-users.component';
import { InactiveUsersComponent } from './inactive-users/inactive-users.component';
import { ShoppingListService } from './shopping-list/shopping-list.service';

const appRoutes: Routes = [
  { path: 'crisis-center', component: SuccessAlertComponent }
  // { path: 'hero/:id',      component: HeroDetailComponent },
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  // { path: '',
  //   redirectTo: '/heroes',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    SignUpFormComponent,
    ServersComponent,
    SuccessAlertComponent,
    WarningalertComponent,
    ModalPopupComponent,
    SortComponent,
    OrderrByPipe,
    CartIconComponent,
    CheckUncheckComponent,
    HeaderComponent,
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    CockpitComponent,
    ServerElementComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    BasicHighlightDirective,
    ParentComponent,
    ChildComponent,
    BetterHighlightDirective,
    DropdownDirective,
    ActiveUsersComponent,
    InactiveUsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //MatButtonModule,MatTooltipModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [ReportService, LoggingService, RecipeService, ShoppingListService],
  bootstrap: [AppComponent]
})



export class AppModule { }
