import { Component, EventEmitter, Output } from '@angular/core';

@Component(
    {
        selector:'app-header',
        templateUrl:'./header.component.html',
    }
)

export class HeaderComponent{
    //Below collapsed is used for Menu bar resposive design for adding/removing the class dynamically.
    collapsed = true;

    @Output() featureSelected = new EventEmitter<string>();

    onSelect(feature: string){
        this.featureSelected.emit(feature);
    }

}